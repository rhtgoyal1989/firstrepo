<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:dp="http://www.datapower.com/extensions" xmlns:dpconfig="http://www.datapower.com/param/config" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx" exclude-result-prefixes="xs dp dpconfig json" extension-element-prefixes="dp dpconfig" version="1.0">
	<xsl:template match="/">
		<!-- Get Request Message from Context Variable -->
		<xsl:variable name="requestMessage">
			<xsl:copy-of select="dp:variable('var://context/Subscription_XMLInterface/requestMessage')"/>
		</xsl:variable>
		<xsl:variable name="var_ABHIErrorCode">
			<xsl:value-of select="'200'"/>
		</xsl:variable>
		<xsl:variable name="var_serviceConfig">
			<xsl:copy-of select="dp:variable('var://context/Subscription_XMLInterface/EAIServiceConfig')"/>
		</xsl:variable>
		<xsl:variable name="var_ABHIErrorMessage">
			<xsl:value-of select="//json:string[@name = 'Message']"/>
		</xsl:variable>
		<xsl:variable name="var_response_message">
			<tns1:READ_WRITE_RESP_GBO xmlns:cmn="http://group.vodafone.com/schema/common/v1" xmlns:hed="http://group.vodafone.com/contract/vho/header/v1" xmlns:vfo="http://group.vodafone.com/contract/vfo/fault/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2" xmlns:extvbo="http://group.vodafone.com/schema/extension/vbo/subscription/subscription/v1" xmlns:cct="urn:un:unece:uncefact:documentation:standard:CoreComponentType:2" xmlns:tns="http://group.vodafone.com/schema/vbm/subscription/subscription/v1" xmlns:vbo="http://group.vodafone.com/schema/vbo/subscription/subscription/v1" xmlns:tns1="http://group.vodafone.com/schema/service/subscription/v1" xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2">
				<Header>
					<xsl:copy-of select="$requestMessage//RouteInfo"/>
					<ResultStatus>
						<wsrf-bf:Timestamp>
							<xsl:value-of select="dp:date('sysdate')"/>
						</wsrf-bf:Timestamp>
						<!-- "ErrorNumber": -->
						<xsl:choose>
							<xsl:when test="/json:object/json:object[1]/json:string[1] = '00'">
								<wsrf-bf:ErrorCode dialect="http://www.oxygenxml.com">
									<xsl:value-of select="'VEAI00000I'"/>
								</wsrf-bf:ErrorCode>
								<wsrf-bf:Description>
									<xsl:value-of select="'0'"/>
								</wsrf-bf:Description>
								<wsrf-bf:Description>
									<xsl:value-of select="'Request has been  processed successfully'"/>
								</wsrf-bf:Description>
								<vfo:Category>
									<xsl:value-of select="'SUCCESS'"/>
								</vfo:Category>
							</xsl:when>
							<xsl:otherwise>
								<wsrf-bf:ErrorCode dialect="http://www.oxygenxml.com">
									<xsl:value-of select="'VEAI34444E'"/>
								</wsrf-bf:ErrorCode>
								<wsrf-bf:Description>
									<xsl:value-of select="'1'"/>
								</wsrf-bf:Description>
								<wsrf-bf:Description>
									<xsl:choose>
										<xsl:when test="/json:object/json:object[1]/json:string[@name='ErrorMessage']!='' or //json:string[@name='Message']!=''">
											<xsl:value-of select="concat(/json:object/json:object[1]/json:string[@name='ErrorMessage'],//json:string[@name='Message'])"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'Unknown Exception from ABHI.'"/>
										</xsl:otherwise>
									</xsl:choose>
								</wsrf-bf:Description>
								<vfo:Category>
									<xsl:value-of select="'FAILURE'"/>
								</vfo:Category>
							</xsl:otherwise>
						</xsl:choose>
					</ResultStatus>
					<xsl:copy-of select="$requestMessage//Correlation"/>
					<xsl:copy-of select="$requestMessage//Destination"/>
					<xsl:copy-of select="$requestMessage//Source"/>
				</Header>
				<UpdateSubscriptionVBMResponse>
					<tns:SubscriptionVBO>
						<!--            Operator-->
						<cmn:Categories>
							<cmn:Category listAgencyName="Operator">
								<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'Operator']]"/>
							</cmn:Category>
						</cmn:Categories>
						<cmn:ValidityPeriod>
							<cmn:FromDate>
								<!--                    RechargeStartDate-->
								<cct:DateString format="format2">
									<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'RechargeStartDate']]"/>
								</cct:DateString>
							</cmn:FromDate>
							<cmn:ToDate>
								<!--                    RechargeEndDate-->
								<cct:DateString format="format3">
									<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'RechargeEndDate']]"/>
								</cct:DateString>
							</cmn:ToDate>
						</cmn:ValidityPeriod>
						<vbo:Details>
							<!--                MSISDN-->
							<vbo:MSISDN>
								<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'MSISDN']]"/>
							</vbo:MSISDN>
							<vbo:Extension>
								<!--                    PartnerID-->
								<extvbo:ExternalID schemeName="PartnerID">
									<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'Partner_ID']]"/>
								</extvbo:ExternalID>
							</vbo:Extension>
						</vbo:Details>
						<vbo:Parts>
							<vbo:CustomerAccounts>
								<vbo:CustomerAccount>
									<cmn:IDs>
										<!--                            VodafoneAccNo-->
										<cmn:ID schemeName="VodafoneAccNo">
											<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'VodaphoneAccNo']]"/>
										</cmn:ID>
										<!--                            Account ID-->
										<cmn:ID schemeName="Account ID">
											<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'Account_ID']]"/>
										</cmn:ID>
									</cmn:IDs>
								</vbo:CustomerAccount>
							</vbo:CustomerAccounts>
							<!--                Recharge MRP-->
							<vbo:Price>
								<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'RechargeMrp']]"/>
							</vbo:Price>
							<vbo:SubscriptionTerm>
								<vbo:PaymentTerms>
									<vbo:Topup>
										<vbo:Options>
											<vbo:Option>
												<!--                                    SUM INSURED PER DAY-->
												<vbo:Amount>
													<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'sumInsured']]"/>
												</vbo:Amount>
											</vbo:Option>
										</vbo:Options>
									</vbo:Topup>
								</vbo:PaymentTerms>
								<!-- <vbo:Extension/> -->
							</vbo:SubscriptionTerm>
						</vbo:Parts>
						<vbo:LineItems>
							<vbo:LineItem>
								<vbo:ProductElement>
									<vbo:CustomerProduct>
										<cmn:IDs>
											<!--                                QuotationNumber-->
											<cmn:ID schemeName="QuotationNumber">
												<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'QuotationNumber']]"/>
											</cmn:ID>
											<!--                                CertificateNumber-->
											<cmn:ID schemeName="CertificateNumber">
												<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'CertificateNumber']]"/>
											</cmn:ID>
										</cmn:IDs>
										<!--                            coiUrl-->
										<cmn:Desc>
											<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'CoiURL']]"/>
										</cmn:Desc>
										<!--                            policyStatus-->
										<cmn:Status>
											<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'policyStatus']]"/>
										</cmn:Status>
										<vbo:ValidityPeriod>
											<cmn:FromDate>
												<!--                                    StartDate-->
												<cct:DateString>
													<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'startDate']]"/>
												</cct:DateString>
											</cmn:FromDate>
											<cmn:ToDate>
												<!--                                    EndDate-->
												<cct:DateString>
													<xsl:value-of select="/*[local-name() = 'object']/*[local-name() = 'object']/*[local-name() = 'string' and @*[local-name() = 'name' and normalize-space(.) = 'EndDate']]"/>
												</cct:DateString>
											</cmn:ToDate>
										</vbo:ValidityPeriod>
									</vbo:CustomerProduct>
								</vbo:ProductElement>
							</vbo:LineItem>
							<vbo:Extension/>
						</vbo:LineItems>
					</tns:SubscriptionVBO>
				</UpdateSubscriptionVBMResponse>
			</tns1:READ_WRITE_RESP_GBO>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(contains($var_ABHIErrorMessage,$var_ABHIErrorCode)) and not(./json:object/json:object[@name='errorObj']/json:string[@name='ErrorNumber'])">
				<xsl:variable name="ConsumerMQMDHeader" select="dp:variable('var://context/context/OriginalMQMD')"/>
				<dp:set-variable name="'var://context/Subscription_XMLInterface/Retry'" value="'Y'"/>
				<xsl:variable name="var_CorrelId">
					<xsl:choose>
						<xsl:when test="$ConsumerMQMDHeader//CorrelId = '000000000000000000000000000000000000000000000000' or $ConsumerMQMDHeader//CorrelId = ''">
							<xsl:value-of select="$ConsumerMQMDHeader//MsgId"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$ConsumerMQMDHeader//CorrelId"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="var_MsgId" select="$ConsumerMQMDHeader//MsgId"/>
				<xsl:variable name="var_QMGr" select="substring-before(substring-after(dp:variable('var://service/URL-in'), 'dpmq://'), '/')"/>
				<xsl:variable name="var_ReplyToQ" select="$ConsumerMQMDHeader//ReplyToQ"/>
				<xsl:variable name="var_mpQueue" select="$var_serviceConfig//MPQueueName"/>
				<xsl:variable name="var_RetryCount" select="$var_serviceConfig//RetryCount"/>
				<dp:set-variable name="'var://context/variable/mpQueue'" value="string($var_mpQueue)"/>
				<dp:set-variable name="'var://context/variable/RetryCount'" value="string($var_RetryCount)"/>
				<xsl:variable name="var_MQRFH2Header" select="dp:request-header('MQRFH2')"/>
				<xsl:variable name="var_parsedHeader" select="dp:parse($var_MQRFH2Header)"/>
				<xsl:variable name="var_mqPutime" select="$var_parsedHeader/MQRFH2/NameValueData/NameValue/*/PutTime/text()"/>
				<xsl:variable name="var_isRetry" select="$var_parsedHeader/MQRFH2/NameValueData/NameValue/*/IsRetry/text()"/>
				<xsl:variable name="var_PutTimeZone" select="dp:variable('var://context/XMLInterface/PutTime')"/>
				<xsl:variable name="Retry">
					<xsl:choose>
						<xsl:when test="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount = '' or not($var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount)">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount &lt;= $var_RetryCount">
							<xsl:value-of select="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount + 1"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<!-- Setting new MQ header -->
				<xsl:variable name="var_newMQRFH2Str">
					<MQRFH2>
						<StrucId>RFH </StrucId>
						<Version>2</Version>
						<Encoding>546</Encoding>
						<CodedCharSetId>819</CodedCharSetId>
						<Format>MQSTR</Format>
						<Flags>0</Flags>
						<NameValueCCSID>1208</NameValueCCSID>
						<NameValueData>
							<NameValue>
								<usr>
									<IsRetry>
										<xsl:value-of select="'Yes'"/>
									</IsRetry>
									<PutTime>
										<xsl:value-of select="$var_PutTimeZone"/>
									</PutTime>
									<RetryCount>
										<xsl:value-of select="$Retry"/>
									</RetryCount>
								</usr>
							</NameValue>
						</NameValueData>
					</MQRFH2>
				</xsl:variable>
				<xsl:variable name="var_serializedMQRFH2">
					<dp:serialize select="$var_newMQRFH2Str" omit-xml-decl="Yes"/>
				</xsl:variable>
				<xsl:variable name="MQMDHeader">
					<MQMD>
						<Expiry>-1</Expiry>
						<Encoding>546</Encoding>
						<CodedCharSetId>819</CodedCharSetId>
						<Format>
							<xsl:value-of select="'MQHRF2'"/>
						</Format>
						<ReplyToQ>
							<xsl:value-of select="$ConsumerMQMDHeader//ReplyToQ"/>
						</ReplyToQ>
						<ReplyToQMgr>
							<xsl:value-of select="''"/>
						</ReplyToQMgr>
						<MsgId>
							<xsl:value-of select="$ConsumerMQMDHeader//MsgId"/>
						</MsgId>
						<CorrelId>
							<xsl:value-of select="$var_CorrelId"/>
						</CorrelId>
					</MQMD>
				</xsl:variable>
				<xsl:variable name="mqmd-serl">
					<dp:serialize select="$MQMDHeader" omit-xml-decl="Yes"/>
				</xsl:variable>
				<xsl:variable name="headers">
					<header name="MQMD">
						<xsl:value-of select="$mqmd-serl"/>
					</header>
					<header name="MQRFH2">
						<xsl:value-of select="$var_serializedMQRFH2"/>
					</header>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$Retry &lt;= $var_RetryCount">
						<dp:set-response-header name="'ReplyToQ'" value="''"/>
						<xsl:variable name="var_backSidePutURL" select="concat('dpmq://', $var_QMGr, '/?RequestQueue=', $var_mpQueue, ';ParseHeaders=true')"/>
						<dp:url-open target="{$var_backSidePutURL}" response="responsecode-ignore" http-headers="$headers">
							<xsl:copy-of select="$requestMessage"/>
						</dp:url-open>
					</xsl:when>
					<xsl:otherwise>
						<dp:set-response-header name="'ReplyToQ'" value="''"/>
						<dp:set-response-header name="'ReplyToQM'" value="''"/>
						<dp:set-variable name="'var://context/Subscription_XMLInterface/Retry'" value="'N'"/>
						<xsl:variable name="FinalHeader">
							<MQMD>
								<Expiry>-1</Expiry>
								<Encoding>546</Encoding>
								<CodedCharSetId>819</CodedCharSetId>
								<Format>
									<xsl:value-of select="'MQSTR'"/>
								</Format>
								<ReplyToQ>
									<xsl:value-of select="''"/>
								</ReplyToQ>
								<ReplyToQMgr>
									<xsl:value-of select="''"/>
								</ReplyToQMgr>
								<MsgId>
									<xsl:value-of select="$ConsumerMQMDHeader//MsgId"/>
								</MsgId>
								<CorrelId>
									<xsl:value-of select="$var_CorrelId"/>
								</CorrelId>
							</MQMD>
						</xsl:variable>
						<xsl:variable name="final_mqmd-serl">
							<dp:serialize select="$FinalHeader" omit-xml-decl="Yes"/>
						</xsl:variable>
						<xsl:variable name="ConsumerFinalheaders">
							<header name="MQMD">
								<xsl:value-of select="$final_mqmd-serl"/>
							</header>
						</xsl:variable>
						<xsl:variable name="var_consumerPutURL" select="concat('dpmq://', $var_QMGr, '/?RequestQueue=', $var_ReplyToQ, ';ParseHeaders=true')"/>
						<xsl:variable name="var_out">
							<dp:url-open target="{$var_consumerPutURL}" response="responsecode-ignore" http-headers="$ConsumerFinalheaders">
								<xsl:copy-of select="$var_response_message"/>
							</dp:url-open>
						</xsl:variable>
						<xsl:copy-of select="$var_response_message"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Retry Ends Here -->
			<xsl:otherwise>
				<dp:set-response-header name="'ReplyToQM'" value="''"/>
				<dp:set-variable name="'var://context/Subscription_XMLInterface/Retry'" value="'N'"/>
				<!-- Creating final response Body -->
				<xsl:copy-of select="$var_response_message"/>
				<dp:remove-http-request-header name="MQRFH2"/>
                <xsl:call-template name="UpdateFinalRespMQMDHeader" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

    <xsl:template name="UpdateFinalRespMQMDHeader">
        <xsl:variable name="ConsumerMQMDHeader" select="dp:variable('var://context/Subscription_XMLInterface/OriginalMQMDHeader')"/>
        
        <xsl:variable name="MQMDhdr">
            <xsl:apply-templates select="$ConsumerMQMDHeader/*" mode="header"/>
        </xsl:variable>
        
        <xsl:variable name="mqmd-serl">
            <dp:serialize select="$MQMDhdr" omit-xml-decl="Yes"/>
        </xsl:variable>
        
        <dp:set-response-header name="'MQMD'" value="$mqmd-serl"/>
    </xsl:template>
    
    <xsl:template match="node() | @*" mode="header">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*" mode="header"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="Format" mode="header">
        <Format>
            <xsl:value-of select="'MQSTR'"/>
        </Format>
    </xsl:template>
    
    <xsl:template match="ReplyToQ" mode="header">
        <ReplyToQ>
            <xsl:value-of select="''"/>
        </ReplyToQ>
    </xsl:template>
</xsl:stylesheet>