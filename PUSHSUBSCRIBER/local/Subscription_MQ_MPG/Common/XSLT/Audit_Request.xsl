<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:dp="http://www.datapower.com/extensions"
    xmlns:dpfunc="http://www.datapower.com/extensions/functions"
    xmlns:date="http://exslt.org/dates-and-times"
    xmlns:dyn="http://exslt.org/dynamic" extension-element-prefixes="dp dpfunc dyn date"
    exclude-result-prefixes="xs dp dpfunc dyn date">
    
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <xsl:variable name="ConsumerMQMDHeader"
            select="dp:parse(dp:request-header('MQMD'))"/>
        <dp:set-variable name="'var://context/context/OriginalMQMD'" value="$ConsumerMQMDHeader" />
        
        <xsl:variable name="var_serviceConfig">
            <xsl:copy-of select="dp:variable('var://context/Subscription_XMLInterface/EAIServiceConfig')" />
        </xsl:variable>
        
        <xsl:variable name="MQRFH2" select="dp:request-header('MQRFH2')"/>
        <xsl:variable name="MQRFH2Header">
            <xsl:choose>
                <xsl:when test="$MQRFH2!=''">
                    <xsl:copy-of select="dp:parse($MQRFH2)"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        
        <dp:set-variable name="'var://context/context/var_currentTime'" value="string($var_currentTime)"></dp:set-variable>
        <xsl:variable name="var_mqPutime" select="$MQRFH2Header//MQRFH2/NameValueData/NameValue/*/PutTime/text()"/>
        <xsl:variable name="var_isRetry" select="$MQRFH2Header//MQRFH2/NameValueData/NameValue/*/IsRetry/text()" />
        <xsl:variable name="var_RetryCount" select="$MQRFH2Header//MQRFH2/NameValueData/NameValue/*/RetryCount/text()" />
        <dp:set-variable name="'var://context/XMLInterface/PutTime'" value="$var_mqPutime" />
        
        <xsl:choose>
            <xsl:when test="$var_isRetry='Yes'">
                <xsl:choose>
                    <xsl:when test="$var_RetryCount &lt;= $var_serviceConfig//RetryCount">
                        <dp:set-variable name="'var://context/XMLInterface/IsRetry'" value="'Yes'" />
                    </xsl:when>
                    <xsl:otherwise>
                        <dp:set-variable name="'var://context/XMLInterface/IsRetry'" value="'Expired'" />
                        <dp:set-variable name="'var://context/local/error-code'" value="'3022'" />
                        <dp:reject override="false">
                            <xsl:value-of select="'Request Message has Expired.'" />
                        </dp:reject>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="var_header" select="dp:parse(dp:request-header('MQMD'))"/>
                <xsl:variable name="var_mqPutDate" select="$var_header//PutDate" />
                <xsl:variable name="var_mqPutTime" select="$var_header//PutTime" />
                
                <xsl:variable name="var_mqYear" select="substring($var_mqPutDate,1,4)" />
                <xsl:variable name="var_mqMonth" select="substring($var_mqPutDate,5,2)" />
                <xsl:variable name="var_mqDate" select="substring($var_mqPutDate,7,2)" />
                <xsl:variable name="var_putdate" select="concat($var_mqYear,'-',$var_mqMonth,'-',$var_mqDate)" />
                
                <xsl:variable name="var_mqHour" select="substring($var_mqPutTime,1,2)" />
                <xsl:variable name="var_mqMinute" select="substring($var_mqPutTime,3,2)" />
                <xsl:variable name="var_mqSecond" select="substring($var_mqPutTime,5,2)" />
                <xsl:variable name="var_mqMSecond" select="substring($var_mqPutTime,7)" />
                <xsl:variable name="var_puttime" select="concat($var_mqHour,':',$var_mqMinute,':',$var_mqSecond)" />
                <xsl:variable name="var_PutTimeZone" select="concat($var_putdate,'T',$var_puttime,'Z')" />
                <dp:set-variable name="'var://context/XMLInterface/PutTime'" value="$var_PutTimeZone" />
            </xsl:otherwise>
        </xsl:choose>
        
        
        <xsl:variable name="Var_Req" select="." />
        <xsl:variable name="var_serl_msg">
            <dp:serialize select="$Var_Req" />
        </xsl:variable> 
        
        <xsl:variable name="var_serl_msg_Backend">
            <xsl:copy-of select="dp:variable('var://context/Request_Transform_Output')"/>
        </xsl:variable>
        <xsl:variable name="var_serl_msg1">
            <dp:serialize select="$var_serl_msg_Backend" omit-xml-decl="yes"/>
        </xsl:variable>
        
        <xsl:variable name="var_reqMessage" select="."/>
        <xsl:variable name="var_ruleType"
            select="dp:variable('var://service/transaction-rule-type')"/>
        <xsl:variable name="var_auditCondition"
            select="string-length(dp:variable('var://context/CircleId_Mapping_Lookup'))"/>
        <xsl:variable name="var_ServiceLookup" select="document('local:///Subscription_MQ_MPG/Common/Config_Xml/EAI_ServiceLookup.xml')"/>
        <xsl:variable name="Servicename1" select="/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'MetaInfo']/*[local-name() = 'ConsumerReqInfo']/*[local-name() = 'RouteInfo']/*[local-name() = 'Route']/*[local-name()='ID']"/>
        <xsl:variable name="Servicename2" select="/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'RouteInfo']/*[local-name() = 'Route']/*[local-name()='ID']"/>
        
        <xsl:variable name="destinationServiceName" select="dp:variable('var://context/Subscription_XMLInterface/ProviderserviceName')"/>
        
        <xsl:variable name="var_commonServiceName">
            <xsl:choose>
                <xsl:when test="string-length(normalize-space($Servicename1))= 0 ">
                    <xsl:value-of select="$Servicename2"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Servicename1"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="VAR_IS_NOTIFY" select="$var_ServiceLookup/EAI_Lookup_Config/services/service[@name=$destinationServiceName]/IS_NOTIFY"/>
        <xsl:choose>
            <!-- IF IT IS NOTIFY API IT WILL CALL 'WHEN' BLOCK, IF IT IS READ AND WRITE API IT WILL CALL 'OTHERWISE' BLOCK -->
            <xsl:when test="$VAR_IS_NOTIFY = 'YES'">
                
                <xsl:variable name="auditMessage">
                    <MetaInfo>
                        <ConsumerReqInfo>
                            <circleId>
                                <xsl:value-of select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Source']/*[local-name() = 'Division']"/>
                            </circleId>
                            <serviceName>
                                <xsl:value-of
                                    select="concat($var_commonServiceName, ':', dp:variable('var://context/Subscription_XMLInterface/ProviderserviceName'))"
                                />
                            </serviceName>
                            <channelName>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Source']/*[local-name() = 'System']"
                                />
                            </channelName>
                            <segment>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'RouteInfo']/*[local-name() = 'Route']/*[local-name() = 'Keys']/*[local-name() = 'Key']"
                                />
                            </segment>
                            <key>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Correlation']/*[local-name() = 'ConversationID']"
                                />
                            </key>
                        </ConsumerReqInfo>
                        <ESBCorrelationInfo>
                            <correlId>
                                <xsl:value-of select="dp:variable('var://context/Subscription_XMLInterface/CorrelId')"
                                />
                            </correlId>
                            <msaKeyData>                  
                                <xsl:value-of select="dp:variable('var://context/Subscription_XMLInterface/msaKeyData')"/>
                            </msaKeyData>
                        </ESBCorrelationInfo>
                        <AuditInfo>
                            <appName>
                                <xsl:value-of select="dp:variable('var://service/processor-name')"/>
                                <xsl:value-of
                                    select="concat('Domain: ', dp:variable('var://service/domain-name'), '  ')"
                                />
                            </appName>
                            <serverName>
                                <xsl:value-of
                                    select="dp:variable('var://service/system/ident')/identification/serial-number"
                                />
                            </serverName>
                            <xsl:choose>
                                
                                <xsl:when test="$var_auditCondition = '0'">
                                    <msaFlowDir>PROVIDER_TO_ESB</msaFlowDir>
                                    <level>PROVIDER_TO_ESB_Level_1</level>
                                    <msaStatus>PUBLISH</msaStatus>
                                </xsl:when>
                                <xsl:otherwise>
                                    <msaFlowDir>ESB_TO_CONSUMER</msaFlowDir>
                                    <level>ESB_TO_CONSUMER_Level_2</level>
                                    <msaStatus>PUBLISH</msaStatus>
                                </xsl:otherwise>
                            </xsl:choose>
                        </AuditInfo>
                        <AuditMessage>
                            <xsl:choose>
                                <xsl:when test="$var_auditCondition='0'">                 
                                    <ConsumerToESB_request>
                                        <xsl:value-of select="$var_serl_msg"/>
                                    </ConsumerToESB_request>
                                </xsl:when>
                                <xsl:otherwise>
                                    <ESBToProvider_request>
                                        <xsl:value-of select="$var_serl_msg1"/>
                                    </ESBToProvider_request>
                                </xsl:otherwise>
                            </xsl:choose>
                        </AuditMessage>
                        <xsl:if test="$var_auditCondition != '0'">
                            <StatusInfo>
                                <errorCode>VEAI00000I</errorCode>
                                <errorStatus>0</errorStatus>
                                <errorDesc>The request message has been processed successfully</errorDesc>
                                <errorCategory>SUCCESS</errorCategory>
                            </StatusInfo>
                        </xsl:if>
                    </MetaInfo>
                </xsl:variable>
                <xsl:copy-of select="$auditMessage"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="auditMessage">
                    <MetaInfo>
                        <ConsumerReqInfo>
                            <circleId>
                                <xsl:value-of select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Source']/*[local-name() = 'Division']"/>
                            </circleId>
                            <serviceName>
                                <xsl:value-of
                                    select="concat($var_commonServiceName, ':', dp:variable('var://context/Subscription_XMLInterface/ProviderserviceName'))"
                                />
                            </serviceName>
                            <channelName>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Source']/*[local-name() = 'System']"
                                />
                            </channelName>
                            <segment>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'RouteInfo']/*[local-name() = 'Route']/*[local-name() = 'Keys']/*[local-name() = 'Key']"
                                />
                            </segment>
                            <key>
                                <xsl:value-of
                                    select="$var_reqMessage/*[local-name() = 'READ_WRITE_REQ_GBO']/*[local-name() = 'Header']/*[local-name() = 'Correlation']/*[local-name() = 'ConversationID']"
                                />
                            </key>
                        </ConsumerReqInfo>
                        <ESBCorrelationInfo>
                            <correlId>
                                <xsl:value-of select="dp:variable('var://context/Subscription_XMLInterface/CorrelId')"
                                />
                            </correlId>
                            <msaKeyData>                  
                                <xsl:value-of select="dp:variable('var://context/Subscription_XMLInterface/msaKeyData')"/>
                            </msaKeyData>
                        </ESBCorrelationInfo>
                        <AuditInfo>
                            <appName>
                                <xsl:value-of select="dp:variable('var://service/processor-name')"/>
                                <xsl:value-of
                                    select="concat('Domain: ', dp:variable('var://service/domain-name'), '  ')"
                                />
                            </appName>
                            <serverName>
                                <xsl:value-of
                                    select="dp:variable('var://service/system/ident')/identification/serial-number"
                                />
                            </serverName>
                            <xsl:choose>
                                <xsl:when test="$var_auditCondition = '0'">
                                    <xsl:choose>
                                        <xsl:when test="dp:variable('var://context/XMLInterface/IsRetry')='Yes'">
                                            <msaFlowDir>MPQ_TO_ESB</msaFlowDir>
                                            <level>CONSUMER_TO_ESB_Level_1</level>
                                            <msaStatus>RETRY</msaStatus>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <msaFlowDir>CONSUMER_TO_ESB</msaFlowDir>
                                            <level>CONSUMER_TO_ESB_Level_1</level>
                                            <msaStatus>REQUEST</msaStatus>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                    <msaFlowDir>ESB_TO_PROVIDER</msaFlowDir>
                                    <level>ESB_TO_PROVIDER_Level_2</level>
                                    <msaStatus>REQUEST</msaStatus>
                                </xsl:otherwise>
                            </xsl:choose>
                        </AuditInfo>
                        <AuditMessage>
                            <xsl:choose>
                                <xsl:when test="$var_auditCondition='0'">                 
                                    <ConsumerToESB_request>
                                        <xsl:value-of select="$var_serl_msg"/>
                                    </ConsumerToESB_request>
                                </xsl:when>
                                <xsl:otherwise>
                                    <ESBToProvider_request>
                                        <xsl:value-of select="$var_serl_msg1"/>
                                    </ESBToProvider_request>
                                </xsl:otherwise>
                            </xsl:choose>
                        </AuditMessage>
                    </MetaInfo>
                </xsl:variable>
                <xsl:copy-of select="$auditMessage"/>
            </xsl:otherwise>
        </xsl:choose>
        
        
    </xsl:template>
</xsl:stylesheet>