<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dp="http://www.datapower.com/extensions"
    xmlns:dpconfig="http://www.datapower.com/param/config"
    xmlns:date="http://exslt.org/dates-and-times"
    extension-element-prefixes="dp date" exclude-result-prefixes="dp dpconfig date">

	<xsl:output method="xml" />

	<xsl:variable name="var_correlId" select="dp:variable('var://context/Subscription_XMLInterface/CorrelId')"/>
	<xsl:variable name="var_MsgId" select="dp:variable('var://context/Subscription_XMLInterface/MsgId')"/>
	<xsl:variable name="var_ReplyToQmgr" select="dp:variable('var://context/Subscription_XMLInterface/ReplyToQmgr')"/>
	<xsl:variable name="var_ReplyToQ" select="dp:variable('var://context/Subscription_XMLInterface/ReplyToQ')"/>

	<xsl:template match="/">

		<dp:set-response-header name="'ReplyToQM'" value="''" />
		<xsl:variable name="var_QMGr" select="substring-before(substring-after(dp:variable('var://service/URL-in'),'dpmq://'),'/')" />

		<dp:set-variable name="'var://context/local/ReplyToQmgr'" value="$var_QMGr" />

		<xsl:variable name="var_eaiErrorLookup" select="document('local:///Subscription_MQ_MPG/Common/Config_Xml/EAI_error_lookup.xml')" />

		<xsl:variable name="var_errorCode">
			<xsl:copy-of select="dp:variable('var://service/error-code')" />
		</xsl:variable>

		<xsl:variable name="var_serviceConfig">
			<xsl:copy-of select="dp:variable('var://context/Subscription_XMLInterface/EAIServiceConfig')" />
		</xsl:variable>

		<xsl:variable name="var_mpQueue" select="$var_serviceConfig//MPQueueName" />

		<xsl:variable name="var_RetryCount" select="$var_serviceConfig//RetryCount"/>

		<dp:set-variable name="'var://context/variable/mpQueue'" value="string($var_mpQueue)" />
		<dp:set-variable name="'var://context/variable/RetryCount'" value="string($var_RetryCount)" />

		<xsl:variable name="var_msgIsRetry">
			<xsl:value-of select="$var_eaiErrorLookup/ExceptionDetail/DPErrorCode[@value=$var_errorCode]/isRetry/text()" />
		</xsl:variable>

		<dp:set-variable name="'var://context/XMLInterface/retry'" value="string($var_isRetry)" />
		<!--<xsl:variable name="var_retrycodes" select="'0x01130006'" />-->
		<xsl:variable name="var_retrycodes" select="'0x01130006'" />

		<xsl:choose>
			<xsl:when test="contains($var_retrycodes,dp:variable('var://service/error-code')) and $var_serviceConfig//Retry='Y'">

				<xsl:variable name="var_MQRFH2Header" select="dp:request-header('MQRFH2')" />
				<xsl:variable name="var_parsedHeader" select="dp:parse($var_MQRFH2Header)" />

				<xsl:variable name="var_mqPutime" select="$var_parsedHeader/MQRFH2/NameValueData/NameValue/*/PutTime/text()" />
				<xsl:variable name="var_isRetry" select="$var_parsedHeader/MQRFH2/NameValueData/NameValue/*/IsRetry/text()" />

				<xsl:call-template name="UpdateMQMDHeader" />

				<xsl:variable name="var_PutTimeZone" select="date:date-time()" />

				<xsl:variable name="Retry">
					<xsl:choose>
						<xsl:when test="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount = '' or not($var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount)">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount &lt;= $var_RetryCount">
							<xsl:value-of select="$var_parsedHeader//MQRFH2/NameValueData/NameValue/*/RetryCount + 1"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>

				<xsl:variable name="var_newMQRFH2Str">
					<MQRFH2>
						<Version>2</Version>
						<Format>MQSTR</Format>
						<NameValueData>
							<NameValue>
								<usr>
									<IsRetry>
										<xsl:value-of select="'Yes'" />
									</IsRetry>
									<PutTime>
										<xsl:value-of select="$var_PutTimeZone" />
									</PutTime>
									<RetryCount>
										<xsl:value-of select="$Retry"/>
									</RetryCount>
								</usr>
							</NameValue>
						</NameValueData>
					</MQRFH2>
				</xsl:variable>

				<xsl:variable name="var_serializedMQRFH2">
					<dp:serialize select="$var_newMQRFH2Str" omit-xml-decl="Yes"/>
				</xsl:variable>

				<xsl:variable name="var_reqMsg" select ="dp:variable('var://context/Subscription_XMLInterface/requestMessage')" />

				<xsl:variable name="var_backSidePutURL" select="concat('dpmq://',$var_QMGr,'/?RequestQueue=',$var_mpQueue)" />

				<dp:set-variable name="'var://context/Customer_XMLInterface/backside_url'" value="string($var_backSidePutURL)"/>

				<dp:set-response-header name="'MQRFH2'" value="$var_serializedMQRFH2" />
				<dp:set-response-header name="'ReplyToQ'" value="$var_mpQueue" />

				<xsl:copy-of select="$var_reqMsg" />

			</xsl:when>
			<xsl:otherwise>
				<dp:remove-http-request-header name="MQRFH2"/>
				<xsl:copy-of select="."/>
				<xsl:call-template name="UpdateFinalRespMQMDHeader" />
			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>

	<xsl:template name="UpdateMQMDHeader">
		<xsl:variable name="ConsumerMQMDHeader" select="dp:variable('var://context/Subscription_XMLInterface/OriginalMQMDHeader')"/>

		<xsl:variable name="MQMDhdr">
			<xsl:apply-templates select="$ConsumerMQMDHeader/*" mode="header"/>
		</xsl:variable>

		<xsl:variable name="mqmd-serl">
			<dp:serialize select="$MQMDhdr" omit-xml-decl="Yes"/>
		</xsl:variable>

		<dp:set-response-header name="'MQMD'" value="$mqmd-serl"/>
	</xsl:template>

	<xsl:template match="node() | @*" mode="header">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="header"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Format" mode="header">
		<Format>
			<xsl:value-of select="'MQHRF2'"/>
		</Format>
	</xsl:template>

	<xsl:template match="CorrelId" mode="header">
		<CorrelId>
			<xsl:value-of select="$var_correlId"/>
		</CorrelId>
	</xsl:template>

	<xsl:template match="ReplyToQMgr" mode="header">
		<ReplyToQMgr>
			<xsl:value-of select="$var_ReplyToQmgr"/>
		</ReplyToQMgr>
	</xsl:template>

	<xsl:template match="ReplyToQ" mode="header">
		<ReplyToQ>
			<xsl:value-of select="$var_ReplyToQ"/>
		</ReplyToQ>
	</xsl:template>

	<xsl:template name="UpdateFinalRespMQMDHeader">
		<xsl:variable name="ConsumerMQMDHeader" select="dp:variable('var://context/Subscription_XMLInterface/OriginalMQMDHeader')"/>

		<xsl:variable name="MQMDhdr">
			<xsl:apply-templates select="$ConsumerMQMDHeader/*" mode="header2"/>
		</xsl:variable>

		<xsl:variable name="mqmd-serl">
			<dp:serialize select="$MQMDhdr" omit-xml-decl="Yes"/>
		</xsl:variable>

		<dp:set-response-header name="'MQMD'" value="$mqmd-serl"/>
	</xsl:template>

	<xsl:template match="node() | @*" mode="header2">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="header2"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Format" mode="header2">
		<Format>
			<xsl:value-of select="'MQSTR'"/>
		</Format>
	</xsl:template>

	<xsl:template match="ReplyToQ" mode="header2">
		<ReplyToQ>
			<xsl:value-of select="''"/>
		</ReplyToQ>
	</xsl:template>


</xsl:stylesheet>